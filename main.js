const BACK_TO_BACK = true;
const FULL_INK = false;

//

const csvString = await fetch("wnrs.csv").then((res) => res.text());
const data = Papa.parse(csvString).data.filter((pair) => pair[0] && pair[1]);

const e = (tag) => document.createElement(tag);

function createTable() {
  let currentTable = e("table");
  let currentTbody = e("tbody");
  currentTable.className = "grid";
  currentTable.append(currentTbody);
  return [currentTable, currentTbody];
}

let [currentTable, currentTbody] = createTable();
let currentRow = e("tr");

let [currentBackTable, currentBackTbody] = createTable();
let currentBackRow = e("tr");

for (let i = 0; i < data.length; i++) {
  const pair = data[i];
  const cell = e("td");
  cell.className = "card";

  let type = e("p");
  if (!BACK_TO_BACK) {
    type.className = "card-type";
    type.innerText = pair[0] || "";
  }

  const contents = e("p");
  contents.className = "card-content";
  contents.innerText = pair[1] || "";

  let watermark = e("p");
  watermark.className = "card-watermark";
  watermark.innerText = "We're not really strangers";

  cell.append(type, contents, watermark);
  currentRow.append(cell);

  if (BACK_TO_BACK) {
    const cell = e("td");
    cell.className = "card card-back " + (FULL_INK ? "full-ink" : "");
    const contents = e("p");
    contents.className = "card-back-content";
    contents.innerText = pair[0] || "";
    cell.append(type, contents);
    currentBackRow.prepend(cell);
  }

  if ((i + 1) % 2 === 0) {
    currentTbody.append(currentRow);
    currentRow = e("tr");

    if (BACK_TO_BACK) {
      currentBackTbody.append(currentBackRow);
      currentBackRow = e("tr");
    }
  }

  if ((i + 1) % 8 === 0) {
    document.body.append(currentTable);
    [currentTable, currentTbody] = createTable();

    if (BACK_TO_BACK) {
      document.body.append(currentBackTable);
      [currentBackTable, currentBackTbody] = createTable();
    }
  }

  if (i === data.length - 1) {
    if (data.length % 2 !== 0) {
      const cell = e("td");
      cell.className = "card";
      currentRow.append(cell);

      if (BACK_TO_BACK) {
        const cell = e("td");
        cell.className = "card";
        currentBackRow.append(cell);
      }
    }

    currentTbody.append(currentRow);
    document.body.append(currentTable);

    if (BACK_TO_BACK) {
      currentBackTbody.append(currentBackRow);
      document.body.append(currentBackTable);
    }
  }
}
