# Printable WNRS

Generates a printable version of the [We're Not Really Strangers](https://www.werenotreallystrangers.com/) card game using HTML as the typesetting tool.

## Generating

1. Tweak the constants in main.js.
   - If the `BACK_TO_BACK` constant is true, the card type will be printed on the back of the page and not right above the card contents.
   - If both the `FULL_INK` and `BACK_TO_BACK` constants are true, the back side of the card will be red with white text.
2. Start a local server on this folder with a tool like [serve](https://github.com/vercel/serve) (run `serve .`) and open it in your browser.
3. Print or save the page as a PDF.

## Developing

- All the cards are fetched and dynamically generated from the `wnrs.csv` file. The left column should be the card type and the right column the contents.
- You can try things out or design with the `testing.html` file, which can also be opened without a server.
